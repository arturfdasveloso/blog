from django.urls import path,include
from . import views
 
urlpatterns = [
  path('',views.home),
  path('postagens/', views.postagem),
  path('postar/', views.novo_post),
  path('editar/post/<int:id_post>/', views.editar_post),
  path('deletar/<int:id_post>/', views.deletar_post),
]
