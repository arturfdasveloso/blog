from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash,get_user_model
from django.contrib import messages
from .forms import UserForm
 
#formulário de criação de usuario
def add_user(request):
   if request.method == 'POST':
       form = UserForm(request.POST)
       if form.is_valid():
           u = form.save()
           u.set_password(u.password)
           u.save()
           return redirect(request.GET.get('next','/accounts/login'))
       else:
           messages.error(request, 'Verifique os dados informados')
   else:
       form = UserForm()
   return render(request, 'accounts/add_usuario.html', {'form': form})
 
#formulário de login
def user_login(request):
   if request.method == 'POST':
       username = request.POST.get('username')
       password = request.POST.get('password')
       user = authenticate(username=username, password=password)
       if user:
           login(request, user)
           return redirect(request.GET.get('next', '/'))
       else:
           messages.error(request, 'Usuário ou senha inválido ou desativado.')
   return render(request, 'accounts/login.html')
 
#logout
def user_logout(request):
   logout(request)
   return redirect(request.GET.get('next','/'))
